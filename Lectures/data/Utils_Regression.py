import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import pandas as pd
import time
import datetime

def OLS_by_hand(X, Y):
    start = time.time()
    N=5
    
    numerator = sum (X*Y) - (1/N)*(sum(X) * sum (Y))
    denominator = sum (X*X) - (1/N)*(sum (X) * sum (X))
    
    slope = numerator / denominator
    
    intercept = np.mean(Y) - slope * np.mean(X)
    
    print('The line that fits the dots is: %.2f + %.2f * X' % (intercept, slope))
    
    f = lambda x: intercept + slope * x
    
    plt.plot( X, Y, 'bo',  markersize=8, color='skyblue', label = "Inputs")
    plt.plot( X, list(map(f, X)), marker='', color='red', linewidth=2, label = "Fitted line")
    plt.legend()
    end = time.time()
    print('OLS_by_hand running time:', str(datetime.timedelta(seconds = end - start)))

# do this with OLS lib as well
def OLS_skLearn(X, Y):
    start = time.time()
    # Reshape your data either using array.reshape(-1, 1)
    # if your data has a single feature or array.reshape(1, -1)
    # if it contains a single sample.
    reg = LinearRegression().fit(X.reshape(-1, 1), Y.reshape(-1, 1))
    print('The line that fits the dots is: %.2f + %.2f * X' % (reg.intercept_, reg.coef_))
    end = time.time()
    print('OLS_skLearn running time:', str(datetime.timedelta(seconds = end - start)))

def cost(theta, X, y):
    """
    """
    m = len(y)
    predictions = X.dot(theta)
    cost = (1/2*m)*np.sum(np.square(predictions - y))
    return cost

def gradient_descent(X, y, theta, learning_rate = 0.01, iterations = 10):
    """
    X = matrix of X
    y = vector of y
    theta = vector of thetas np.random.randn(j, 1)
    learning_rate
    iterations = number of iterations
    """
    start = time.time()
    
    m = len(y)
    cost_history = np.zeros(iterations)
    theta_history = np.zeros((iterations, 2))
    
    for i in range(iterations):
        prediction = np.dot(X, theta)
        theta = theta - (1/m)*learning_rate*(X.T.dot(prediction - y))
        theta_history[i,:] = theta.T
        cost_history[i] = cost(theta, X, y)
        end = time.time()
    
    end = time.time()
    print('gradient_descent running time:', str(datetime.timedelta(seconds = end - start)))
    return theta, cost_history, theta_history, pd.DataFrame(theta_history, columns=['intercept', 'slope'])


# theta, cost_h, theta_h, df = gradient_descent(XX,Y.reshape(-1,1), theta, learning_rate=0.1, iterations=1500)
